package pubquiz;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.Scanner;
import java.util.regex.Matcher;


/**
 *
 * @author PaFi
 */
public class PubQuiz {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException, UnsupportedEncodingException, IOException {
        File f=new File("questions.txt");
        InputStream stream = new FileInputStream(f.getAbsoluteFile());
        Scanner sc=new Scanner(stream);
        String s="";
        while (sc.hasNextLine()) {
                    s += sc.nextLine() ;
                }
        Regexy r=new Regexy();
        r.getQuestions(s);
        r.getAnswers(s);
        r.getSources(s);
        r.getTopics(s);
        r.getPauses(s);
        r.getBackgournds(s);
        
        System.out.println(r.getAllAnswers().size()+"<A|T"+r.getAllTopics().size() +"Q>"+r.getAllQuestions().size() + "P>"+r.getAllPauses().size() +" B>"+r.getAllBackgrounds().size());
       // System.out.println(s);
        int topicCounter=0;
        int pauseCounter=0;
        int urlCounter=0;
        
    for(int i=0;i<r.getAllQuestions().size();i++){
        if(i%10==0&i>5){
                TemplateP p= new TemplateP(r.getAllPauses().get(pauseCounter),urlCounter);
                pauseCounter++;
                urlCounter++;
            }
        if(i%5==0){
            TemplateTopic tp=new TemplateTopic(r.getAllTopics().get(topicCounter),urlCounter,r.getAllBackgrounds().get(topicCounter));
            topicCounter++;
            urlCounter++;  
        }
        Question q=new Question();
        q.setContent(r.getAllQuestions().get(i));
        q.setNumber(i);
        q.setAnswer(r.getAllAnswers().get(i));
        Template t =new Template(q,urlCounter);
        urlCounter++;
        
    }   
    
    
    }
    
   
    
}
