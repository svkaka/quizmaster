/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pubquiz;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;

/**
 *
 * @author PaFi
 */
public class Template {

    public static String beforeQuestion = "<!DOCTYPE html>\n" +
"<html >\n" +
"<head>\n" +
"<meta charset=\"UTF-8\">\n" +
"<title>pubQuiz</title>\n" +
"<link href=\"res/style.css\" rel=\"stylesheet\" type=\"text/css\">\n" +
"<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js\"></script>\n" +
"<script src=\"res/index.js\"></script>\n" +
"</head>\n" +
"\n" +
"<body>\n" +
"<div id=\"logo\"></div>\n" +
"<div id=\"container\">\n" +
"  <div id=\"question\">";

    public static String beforeAnswer = " </div>\n"
            + "  <div id=\"showBut\" onClick=\"showAnswer()\"> <span>Odpoveď</span> </div>\n"
            + "  <div id=\"answer\" > ";

    public static String beforeSrc = "\n"
            + "    ";

    public static String beforeNext = "\n" +
"  </div>\n" +
"</div><a href=\"";

    public static String beforePrew = "\">\n" +
"<div id=\"next\" ></div> </a>\n" +
"<a href=\"";

    public static String beforeEnd = "\">\n" +
"<div id=\"prew\"></div>\n" +
"</a>\n" +
"</body>\n" +
"</html>";

    public Template(Question q, int in) throws FileNotFoundException, UnsupportedEncodingException, IOException {
        Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("pquiz"+(in)+".html"),"utf-8"));
       String s = beforeQuestion;
        s += q.getContent();
        s += beforeAnswer;
        s += q.getAnswer();
        s +=beforeSrc;
       /* s +=q.getAnswer().getSrc();*/
        s += beforeNext;
        s+="pquiz"+(in+1)+".html";
        s += beforePrew;
        s+="pquiz"+(in-1)+".html";
        s += beforeEnd;
        writer.write(s);
        writer.close();
    }

}
