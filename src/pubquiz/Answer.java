package pubquiz;

public class Answer {

        private String src;
        private String content;
        private int number;

        /**
         * @return the content
         */
        public String getContent() {
            return content;
        }

        /**
         * @param content the content to set
         */
        public void setContent(String content) {
            this.content = content;
        }

        /**
         * @return the number
         */
        public int getNumber() {
            return number;
        }

        /**
         * @param number the number to set
         */
        public void setNumber(int number) {
            this.number = number;
        }

        /**
         * @return the src
         */
        public String getSrc() {
            return src;
        }

        /**
         * @param src the src to set
         */
        public void setSrc(String src) {
            this.src = src;
        }

    }