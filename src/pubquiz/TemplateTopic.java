/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pubquiz;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;

/**
 *
 * @author PaFi
 */
public class TemplateTopic {
    public static String tBeforeTopic="<!DOCTYPE html>\n" +
"<html >\n" +
"<head>\n" +
"<meta charset=\"UTF-8\">\n" +
"<title>pubQuiz</title>\n" +
"<link href=\"res/style.css\" rel=\"stylesheet\" type=\"text/css\">\n" +
"<script src=\"res/index.js\"></script>\n" +
"</head>\n" +
"\n" +
"<body>\n" +
"<div id=\"logo\"></div>\n" +
"<div id=\"container\">\n" +
"  <div id=\"topic\"> <span>";
    
    public static String tBeforeBgr="<style>\n" +
"  body{\n" +
"  background:url(";
    
    public static String tBeforeNext=");\n" +
"  background-repeat:space;\n" +
"  	background-size:cover;\n" +
"  opacity:0.5;\n" +
"  }\n" +
"  </style>\n" +
"  </span> </div>\n" +
"  </div>\n" +
"  <a href=\"";
    
    public static String tBeforePrew="\">\n" +
"<div id=\"next\"></div>\n" +
"</a>\n" +
"<a href=\"";
    
    public static String tBeforeEnd="\">\n" +
"<div id=\"prew\"> </div>\n" +
"</a>\n" +
"</body>\n" +
"</html>";
    
    public TemplateTopic(String topic,int in,String bgr) throws FileNotFoundException, UnsupportedEncodingException, IOException{
        Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("pquiz"+(in)+".html"),"utf-8"));
        String s=tBeforeTopic;
        s+=topic;
        s+=tBeforeBgr;
        s+=bgr;
        s+=tBeforeNext;
        s+="pquiz"+(in+1)+".html";
        s+=tBeforePrew;
        s+="pquiz"+(in-1)+".html";
        s+=tBeforeEnd; 
        writer.write(s);
        writer.close();
    }

}
