/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pubquiz;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author PaFi
 */
public class Regexy {
    /*public List <Question> allQuestions;
    public List<Question.Answer> allAnswers;
    public List<Topic> allTopics;*/
    
    private List <String> allQuestions =new ArrayList<>();
    private List<String> allAnswers =new ArrayList<>();
    private List<String> allTopics =new ArrayList<>();
    private List<String> allPauses =new ArrayList<>();
    private List<String> allBackgrounds =new ArrayList<>();
    
    public static String findQaA="(\\[[0-9]+\\].+?\\[[*]\\])";
    public static String findTopic="(\\{t«)(.+?)(»\\})";
    public static String findPause="(\\{«pa)(.+?)(»\\})";
    public static String findQ="(\\[q«)(.+?)(»\\])";
    public static String findA="(\\[a«)(.+?)(»\\])";
    public static String findS="(\\{s«)(.+?)(»\\})";
    
    public static Pattern patternQaA=Pattern.compile("(\\[[0-9]+\\].+?\\[[*]\\])");
    public static Pattern patternTopic=Pattern.compile("(\\{t«)(.+?)(»\\})");
    public static Pattern patternPause=Pattern.compile("(\\{«pa»\\})");
   
    public static Pattern patternQ=Pattern.compile("(\\[q«)(.+?)(»\\])");
    public static Pattern patternA=Pattern.compile("(\\[a«)(.+?)(»\\])");
    public static Pattern patternS=Pattern.compile("(\\{s«)(.+?)(»\\})");
    public static Pattern patternP=Pattern.compile("(\\{«pa)(.*?)(»\\})");
    public static Pattern patternB=Pattern.compile("(\\[b«)(.*?)(»\\])");
    
   /* public static Pattern patternQ=Pattern.compile("(\\\\[q\\u00AB)(.+?)(\\u00BB\\\\])");
    public static Pattern patternA=Pattern.compile("(\\[a\u00ab)(.+?)(\u00bb\\])/sgm");
    public static Pattern patternS=Pattern.compile("(\\[s\u00ab)(.+?)(\u00bb\\])/sgm");
    */
    
     public void getQuestions(String s){
          Matcher matQ=null;
          matQ=Regexy.patternQ.matcher(s);
         while(matQ.find()){
              getAllQuestions().add(matQ.group(2));
          }          
    }
     
     public void getBackgournds(String s){
          Matcher matQ=null;
          matQ=Regexy.patternB.matcher(s);
         while(matQ.find()){
              getAllBackgrounds().add(matQ.group(2)+"");
          }          
    }
    
    public void getAnswers(String s){
        Matcher matQ=null;
          matQ=Regexy.patternA.matcher(s);
         while(matQ.find()){
              getAllAnswers().add(matQ.group(2));
          }     
    }
    
    public void getSources(String s){
        Matcher matQ=null;
          matQ=Regexy.patternS.matcher(s);
         while(matQ.find()){
              getAllQuestions().add(matQ.group(2));
          } 
    }
    
    public void getPauses(String s){
        Matcher matQ=null;
          matQ=Regexy.patternP.matcher(s);
         while(matQ.find()){
              getAllPauses().add(matQ.group(2)+"");
          } 
    }
    
    public void getTopics(String s){ 
        Matcher matQ=null;
          matQ=Regexy.patternTopic.matcher(s);
         while(matQ.find()){
              getAllTopics().add(matQ.group(2));
          } 
    }

    /**
     * @return the allQuestions
     */
    public List <String> getAllQuestions() {
        return allQuestions;
    }

    /**
     * @param allQuestions the allQuestions to set
     */
    public void setAllQuestions(List <String> allQuestions) {
        this.allQuestions = allQuestions;
    }

    /**
     * @return the allAnswers
     */
    public List<String> getAllAnswers() {
        return allAnswers;
    }

    /**
     * @param allAnswers the allAnswers to set
     */
    public void setAllAnswers(List<String> allAnswers) {
        this.allAnswers = allAnswers;
    }

    /**
     * @return the allTopics
     */
    public List<String> getAllTopics() {
        return allTopics;
    }

    /**
     * @param allTopics the allTopics to set
     */
    public void setAllTopics(List<String> allTopics) {
        this.allTopics = allTopics;
    }

    /**
     * @return the allPauses
     */
    public List<String> getAllPauses() {
        return allPauses;
    }

    /**
     * @param allPauses the allPauses to set
     */
    public void setAllPauses(List<String> allPauses) {
        this.allPauses = allPauses;
    }

    /**
     * @return the allBackgrounds
     */
    public List<String> getAllBackgrounds() {
        return allBackgrounds;
    }

    /**
     * @param allBackgrounds the allBackgrounds to set
     */
    public void setAllBackgrounds(List<String> allBackgrounds) {
        this.allBackgrounds = allBackgrounds;
    }
    
}
